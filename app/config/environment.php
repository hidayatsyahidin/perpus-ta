<?php

define("APP_NAME", "Perpustakaan");
define("DEV_NAME", "Hidayat Syahidin Ambo");

define("DB_DRIVER", "mysql");
define("DB_HOST", "localhost");
define("DB_NAME", "library");
define("DB_USERNAME", "root");
define("DB_PASSWORD", "");

define("VIEW_PATH", "../app/views/");

define("DEFAULT_IMAGE", "default.png");
