
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><?= $title ?> &ndash; <?= APP_NAME ?></title>
    <link rel="stylesheet" href="/mazer/css/main/app.css" />
    <link rel="stylesheet" href="/mazer/css/main/app-dark.css">
    <link rel="stylesheet" href="/css/custom.css">
    <link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css'>  
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@500&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/mazer/css/main/app-dark.css">
    <link rel="stylesheet" href="/css/custom.css">
    <style> @import url('https://fonts.googleapis.com/css2?family=Nunito:wght@500&display=swap'); </style>

  </head>

  <body style="font-family: 'Nunito', sans-serif;">